From 4fed232b80fb1ad6c0e1dfb42ed8d8e1e6821dd7 Mon Sep 17 00:00:00 2001
From: Cheyenne Wills <cwills@sinenomine.net>
Date: Mon, 18 Sep 2023 12:23:01 -0600
Subject: [PATCH] Linux 6.6: Pass request_mask to generic_fillattr

The Linux 6.6 commit: "fs: pass the request_mask to generic_fillattr"
(0d72b92883) added an additional parameter to Linux's
generic_fillattr() function.

For openafs, generic_fillattr() is called from the inode_operations
method "getattr", which is implemented in afs_linux_getattr(). The value
for the request_mask parameter is an existing parameter that is passed
to the inode_operations "getattr" method.

Add an autoconf test for 4 parameters to the generic_fillattr function
and update afs_linux_getattr() to pass the request_mask to
generic_fillattr().

Change-Id: Ie1e6b15bd85931debe0fd446760674ddd0492578
Reviewed-on: https://gerrit.openafs.org/15561
Tested-by: BuildBot <buildbot@rampaginggeek.com>
Reviewed-by: Mark Vitale <mvitale@sinenomine.net>
Tested-by: Mark Vitale <mvitale@sinenomine.net>
Reviewed-by: Andrew Deason <adeason@sinenomine.net>
Reviewed-by: Kailas Zadbuke <kailashsz@in.ibm.com>
Reviewed-by: Benjamin Kaduk <kaduk@mit.edu>
---
 src/afs/LINUX/osi_vnodeops.c    |  4 ++++
 src/cf/linux-kernel-assorted.m4 |  1 +
 src/cf/linux-test4.m4           | 13 +++++++++++++
 3 files changed, 18 insertions(+)

diff --git a/src/afs/LINUX/osi_vnodeops.c b/src/afs/LINUX/osi_vnodeops.c
index a537a72..0f48417 100644
--- a/src/afs/LINUX/osi_vnodeops.c
+++ b/src/afs/LINUX/osi_vnodeops.c
@@ -1188,7 +1188,11 @@ afs_linux_getattr(struct mnt_idmap *idmap, const struct path *path, struct kstat
 {
 	int err = afs_linux_revalidate(path->dentry);
 	if (!err) {
+# if defined(GENERIC_FILLATTR_TAKES_REQUEST_MASK)
+		generic_fillattr(afs_mnt_idmap, request_mask, path->dentry->d_inode, stat);
+# else
 		generic_fillattr(afs_mnt_idmap, path->dentry->d_inode, stat);
+# endif
 	}
 	return err;
 }
diff --git a/src/cf/linux-kernel-assorted.m4 b/src/cf/linux-kernel-assorted.m4
index d8d0aa5..31e870c 100644
--- a/src/cf/linux-kernel-assorted.m4
+++ b/src/cf/linux-kernel-assorted.m4
@@ -58,6 +58,7 @@ LINUX_IOP_LOOKUP_TAKES_UNSIGNED
 LINUX_D_INVALIDATE_IS_VOID
 LINUX_KERNEL_READ_OFFSET_IS_LAST
 LINUX_KEYRING_SEARCH_TAKES_RECURSE
+LINUX_GENERIC_FILLATTR_TAKES_REQUEST_MASK
 ])
 
 
diff --git a/src/cf/linux-test4.m4 b/src/cf/linux-test4.m4
index 1e4dcaf..3596b6a 100644
--- a/src/cf/linux-test4.m4
+++ b/src/cf/linux-test4.m4
@@ -854,3 +854,16 @@ AC_DEFUN([LINUX_KEYRING_SEARCH_TAKES_RECURSE], [
                        [define if your keyring_search has the recurse parameter],
                        [])
 ])
+
+dnl Linux 6.6 added the 'request_mask' parameter to generic_fillattr.
+AC_DEFUN([LINUX_GENERIC_FILLATTR_TAKES_REQUEST_MASK], [
+  AC_CHECK_LINUX_BUILD([whether generic_fillattr has the request_mask parameter],
+                       [ac_cv_linux_func_generic_fillattr_takes_request_mask],
+                       [#include <linux/fs.h>],
+                       [
+                       generic_fillattr(NULL, 0, NULL, NULL);
+                       ],
+                       [GENERIC_FILLATTR_TAKES_REQUEST_MASK],
+                       [define if your generic_fillattr has the request_mask_parameter],
+                       [])
+])
-- 
1.9.4

