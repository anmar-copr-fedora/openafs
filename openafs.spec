%define thiscell openafs.org

%ifarch %{ix86}
%define sysname i386_linux26
%endif
%ifarch ppc ppc64
%define sysname ppc_linux26
%endif
%ifarch x86_64
%define sysname amd64_linux26
%endif

%define pre %nil

%define dkms_version %{version}-%{release}

# Use systemd unit files on RHEL 7 and above.
%if 0%{fedora} || 0%{?rhel} >= 7
  %global _with_systemd 1
%endif

%global debug_package %{nil}

%global _default_patch_fuzz 2

Summary:        Enterprise Network File System
Name:           openafs
Version:        1.8.10
Release:        %{?pre:0.}3%{?pre}%{?dist}
License:        IBM Public License
Group:          System Environment/Daemons
URL:            http://www.openafs.org
%if %{?pre:1}
Source0:        http://dl.openafs.org/dl/candidate/%{version}%{pre}/%{name}-%{version}%{pre}-src.tar.bz2
Source1:        http://dl.openafs.org/dl/candidate/%{version}%{pre}/%{name}-%{version}%{pre}-doc.tar.bz2
%else
Source0:        http://dl.openafs.org/dl/%{version}/%{name}-%{version}-src.tar.bz2
Source1:        http://dl.openafs.org/dl/%{version}/%{name}-%{version}-doc.tar.bz2
%endif

Source9:        http://www.openafs.org/dl/openafs/%{version}/ChangeLog
Source10:       http://www.openafs.org/dl/openafs/%{version}/RELNOTES-%{version}

Source11:       https://www.central.org/dl/cellservdb/CellServDB.2022-05-09
Source12:       cacheinfo
Source13:       openafs.init
Source14:       afs.conf
# Similar to afs.conf, but for systemd.
Source15:       afs.conf.systemd
# Handle AFS post init scriptlets for systemd
Source16:       posthooks.sh
# Default post init scripts
Source20:       sysnames.sh
Source21:       setcrypt.sh

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  krb5-devel, pam-devel, ncurses-devel, flex, byacc, bison
BuildRequires:  automake, autoconf, libtool
BuildRequires:  %{_bindir}/pod2man
%if 0%{?_with_systemd}
BuildRequires: systemd-units
%endif

Patch0:         openafs-1.8.2-fPIC.patch
# systemd: Skip CellServDB manipulation
Patch1:        openafs-1.6.6-systemd-no-cellservdb.patch
# systemd: unload the proper kernel module
Patch2:        openafs-1.6.6-systemd-kmod-name.patch
# systemd: use FHS-style paths instead of transarc paths
Patch3:        openafs-1.6.6-systemd-fhs.patch
# systemd: add additional user-friendly environment vars
Patch4:        openafs-1.6.6-systemd-env-vars.patch
# Add ExecPostStart "sysnames" helper script.
Patch5:        openafs-1.6.6-systemd-execpoststart.patch

Patch100:      openafs-1.8.10-linux65-a.patch
Patch101:      openafs-1.8.10-linux65-b.patch
Patch102:      openafs-1.8.10-linux65-c.patch
Patch103:      openafs-1.8.10-linux65-d.patch
Patch104:      openafs-1.8.10-linux65-e.patch
Patch110:      openafs-1.8.10-linux66-a.patch
Patch111:      openafs-1.8.10-linux66-b.patch
Patch112:      openafs-1.8.10-linux66-c.patch

%description
The AFS distributed filesystem.  AFS is a distributed filesystem
allowing cross-platform sharing of files among multiple computers.
Facilities are provided for access control, authentication, backup and
administrative management.

This package provides common files shared across all the various
OpenAFS packages but are not necessarily tied to a client or server.


%package client
Summary:        OpenAFS Filesystem client
Group:          System Environment/Daemons
Requires(post): bash, coreutils, selinux-policy-targeted
%if 0%{?_with_systemd}
Requires(preun): systemd
Requires(postun): systemd
Requires(post): systemd
%else
Requires(post): chkconfig
%endif
Requires:       %{name}-kmod  >= %{version}
Requires:       openafs = %{version}
Provides:       %{name}-kmod-common = %{version}

%description client
The AFS distributed filesystem.  AFS is a distributed filesystem
allowing cross-platform sharing of files among multiple computers.
Facilities are provided for access control, authentication, backup and
administrative management.

This package provides basic client support to mount and manipulate
AFS.  


%package devel
Summary:        OpenAFS development header files and static libraries
Group:          Development/Libraries
Requires:       openafs = %{version}-%{release}
Requires(post): /sbin/ldconfig
Provides:       openafs-static = %{version}-%{release}
 
%description devel
The AFS distributed filesystem.  AFS is a distributed filesystem
allowing cross-platform sharing of files among multiple computers.
Facilities are provided for access control, authentication, backup and
administrative management.

This package provides static development libraries and headers needed
to compile AFS applications.  Note: AFS currently does not provide
shared libraries.

 
%package server
Summary:    OpenAFS Filesystem Server
Group:      System Environment/Daemons
Requires:   openafs-client = %{version}, openafs = %{version}
%if 0%{?_with_systemd}
Requires(preun): systemd
Requires(postun): systemd
Requires(post): systemd
%endif
 
%description server
The AFS distributed filesystem.  AFS is a distributed filesystem
allowing cross-platform sharing of files among multiple computers.
Facilities are provided for access control, authentication, backup and
administrative management.

This package provides basic server support to host files in an AFS
Cell.

%package -n dkms-%{name}
Summary:        DKMS-ready kernel source for AFS distributed filesystem
Group:          Development/Kernel
Provides: %{name}-kmod = %{version}
Requires:       kernel-devel
Requires(pre):  dkms
Requires(pre):  flex
Requires(post): dkms

%description -n dkms-%{name}
The AFS distributed filesystem.  AFS is a distributed filesystem
allowing cross-platform sharing of files among multiple computers.
Facilities are provided for access control, authentication, backup and
administrative management.

This package provides the source code to allow DKMS to build an
AFS kernel module.


%prep
%setup -q -b 1 -n openafs-%{version}%{pre}

# This changes osconf.m4 to build with -fPIC on i386 and x86_64
%patch -P 0 -p1 -b .fpic

# systemd unit file changes for RPM Fusion
%patch -P 1 -p1 -b .cellservdb
%patch -P 2 -p1 -b .kmod
%patch -P 3 -p1 -b .fhs
%patch -P 4 -p1 -b .envvars
%patch -P 5 -p1 -b .execpoststart

%patch -P 100 -p1 -b .linux65a
%patch -P 101 -p1 -b .linux65b
%patch -P 102 -p1 -b .linux65c
%patch -P 103 -p1 -b .linux65d
%patch -P 104 -p1 -b .linux65e
%patch -P 110 -p1 -b .linux66a
%patch -P 111 -p1 -b .linux66b
%patch -P 112 -p1 -b .linux66c

# Convert the licese to UTF-8
mv LICENSE LICENSE~
iconv -f ISO-8859-1 -t UTF8 LICENSE~ > LICENSE
rm LICENSE~

%build

# do the main build
buildIt() {
./regen.sh

# build the user-space bits for base architectures
    ./configure \
        --prefix=%{_prefix} \
        --libdir=%{_libdir} \
        --bindir=%{_bindir} \
        --sbindir=%{_sbindir} \
        --sysconfdir=%{_sysconfdir} \
        --localstatedir=%{_var} \
        --with-afs-sysname=%{sysname} \
        --with-linux-kernel-headers=%{ksource_dir} \
        --disable-kernel-module \
        --disable-strip-binaries \
        --enable-supergroups  \
        --with-krb5-conf=/usr/bin/krb5-config

    # Build is not SMP compliant
    make $RPM_OPT_FLGS all_nolibafs
    # Build the libafs tree
    make $RPM_OPT_FLGS only_libafs_tree
}

buildIt

%install
rm -rf ${RPM_BUILD_ROOT}
make DESTDIR=$RPM_BUILD_ROOT install

# Set the executable bit on libraries in libdir, so rpmbuild knows to 
# create "Provides" entries in the package metadata for the libraries
chmod +x ${RPM_BUILD_ROOT}%{_libdir}/*.so*

# install config info
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/openafs
install -p -m 644 %{SOURCE11} ${RPM_BUILD_ROOT}%{_sysconfdir}/openafs/CellServDB
install -p -m 644 %{SOURCE12} ${RPM_BUILD_ROOT}%{_sysconfdir}/openafs
echo %{thiscell} > ${RPM_BUILD_ROOT}%{_sysconfdir}/openafs/ThisCell

# install the init file(s)
%if 0%{?_with_systemd}
  # install systemd service files
  mkdir -p ${RPM_BUILD_ROOT}%{_unitdir}
  pushd src/packaging/RedHat
  install -p -m 644 openafs-client.service \
    ${RPM_BUILD_ROOT}%{_unitdir}/openafs-client.service
  install -p -m 644 openafs-server.service \
    ${RPM_BUILD_ROOT}%{_unitdir}/openafs-server.service
  popd
  # install "sysnames" helper script
  install -p -m 755 %{SOURCE16} \
    ${RPM_BUILD_ROOT}%{_libexecdir}/openafs/posthooks.sh
  install -d -m 755 \
    ${RPM_BUILD_ROOT}%{_sysconfdir}/openafs/posthooks.d
  install -p -m 644 %{SOURCE20} \
    ${RPM_BUILD_ROOT}%{_sysconfdir}/openafs/posthooks.d/sysnames.sh
  install -p -m 644 %{SOURCE21} \
    ${RPM_BUILD_ROOT}%{_sysconfdir}/openafs/posthooks.d/setcrypt.sh
%else
  # install legacy SysV init script
  mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/rc.d/init.d
  install -m 755 %{SOURCE13} ${RPM_BUILD_ROOT}%{_sysconfdir}/rc.d/init.d/openafs
%endif

# sysconfig file
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig
%if 0%{?_with_systemd}
  install -m 644 %{SOURCE15} ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/openafs
%else
  install -m 644 %{SOURCE14} ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/openafs
%endif

# Include the vlclient binary
install -m 755 src/vlserver/vlclient ${RPM_BUILD_ROOT}/usr/sbin/vlclient

# Rename /usr/bin/backup to not conflict with Coda 
# (Filed upstream as RT #130621)
mv ${RPM_BUILD_ROOT}/usr/sbin/backup ${RPM_BUILD_ROOT}/usr/sbin/afsbackup

# Put the PAM modules in a sane place
mkdir -p ${RPM_BUILD_ROOT}/%{_lib}/security

# Remove utilities related to DCE
rm -f ${RPM_BUILD_ROOT}/usr/bin/dlog
rm -f ${RPM_BUILD_ROOT}/usr/bin/dpass

# Install man pages
tar cf - -C doc/man-pages man1 man5 man8 | \
    tar xf - -C $RPM_BUILD_ROOT%{_mandir}

# remove unused man pages
for x in afs_ftpd afs_inetd afs_login afs_rcp afs_rlogind afs_rsh \
    dkload knfs package runntp symlink symlink_list symlink_make \
    symlink_remove kpasswd; do
        rm -f $RPM_BUILD_ROOT%{_mandir}/man1/${x}.1
done

# rename backup man page to afsbackup
mv $RPM_BUILD_ROOT%{_mandir}/man8/backup.8 \
   $RPM_BUILD_ROOT%{_mandir}/man8/afsbackup.8

# Install documentation.
mkdir -p $RPM_BUILD_ROOT%{_docdir}/openafs-%{version}
install -m 644 %{SOURCE9} $RPM_BUILD_ROOT%{_docdir}/openafs-%{version}
install -m 644 %{SOURCE10} $RPM_BUILD_ROOT%{_docdir}/openafs-%{version}

# Create the cache directory
install -d -m 700 $RPM_BUILD_ROOT%{_localstatedir}/cache/openafs

#
# install dkms source
#
install -d -m 755 $RPM_BUILD_ROOT%{_prefix}/src
cp -a libafs_tree $RPM_BUILD_ROOT%{_prefix}/src/%{name}-%{dkms_version}

cat > $RPM_BUILD_ROOT%{_prefix}/src/%{name}-%{dkms_version}/dkms.conf <<"EOF"

PACKAGE_VERSION="%{dkms_version}"

# Items below here should not have to change with each driver version
PACKAGE_NAME="%{name}"
MAKE[0]='./configure --with-linux-kernel-headers=${kernel_source_dir} --with-linux-kernel-packaging && make && case "${kernelver_array[0]}${kernelver[0]}" in 2.4.*) mv src/libafs/MODLOAD-*/libafs-* openafs.o ;; *) mv src/libafs/MODLOAD-*/openafs.ko . ;; esac'
CLEAN="make -C src/libafs clean"

BUILT_MODULE_NAME[0]="$PACKAGE_NAME"
DEST_MODULE_LOCATION[0]="/kernel/3rdparty/$PACKAGE_NAME/"
STRIP[0]=no
AUTOINSTALL=yes

EOF

# don't restart in post because kernel modules could well have changed
%post
/sbin/ldconfig
%if 0%{?_with_systemd}
  # systemd unit files are in the -client and -server subpackages.
%else
  if [ $1 = 1 ]; then
        /sbin/chkconfig --add openafs
  fi
%endif

%post client
%if 0%{?_with_systemd}
  %systemd_post openafs-client.service
%endif
# if this is owned by the package, upgrades with afs running can't work
if [ ! -d /afs ] ; then
        mkdir -m 700 /afs
        [ -x /sbin/restorecon ] && /sbin/restorecon /afs
fi 
exit 0

%post server
%if 0%{?_with_systemd}
  %systemd_post openafs-server.service
%endif

%post -n dkms-%{name}
dkms add -m %{name} -v %{dkms_version} --rpm_safe_upgrade
dkms build -m %{name} -v %{dkms_version} --rpm_safe_upgrade
dkms install -m %{name} -v %{dkms_version} --rpm_safe_upgrade

%preun
%if 0%{?_with_systemd}
  # systemd unit files are in the -client and -server subpackages.
%else
  if [ "$1" = 0 ] ; then
        /sbin/chkconfig --del openafs
        %{_sysconfdir}/rc.d/init.d/openafs stop && rmdir /afs
  fi
  exit 0
%endif

%preun client
%if 0%{?_with_systemd}
  %systemd_preun openafs-client.service
%endif

%preun server
%if 0%{?_with_systemd}
  %systemd_preun openafs-server.service
%endif

%preun -n dkms-%{name}
dkms remove -m %{name} -v %{dkms_version} --rpm_safe_upgrade --all ||:

%postun -p /sbin/ldconfig

%postun client
%if 0%{?_with_systemd}
  %systemd_postun openafs-client.service
%endif

%postun server
%if 0%{?_with_systemd}
  %systemd_postun openafs-server.service
%endif

%post devel -p /sbin/ldconfig

%postun devel -p /sbin/ldconfig

%clean
rm -fr $RPM_BUILD_ROOT

%files
%defattr(-, root, root, -)
%dir %{_sysconfdir}/openafs
%dir %{_libexecdir}/openafs
%dir %{_docdir}/openafs-%{version}
%doc LICENSE README NEWS
%doc README-WINDOWS
%doc %{_docdir}/openafs-%{version}/*
%config(noreplace) %{_sysconfdir}/sysconfig/*
%if 0%{?_with_systemd}
# systemd files are in -client and -server subpackages
%else
%{_sysconfdir}/rc.d/init.d/*
%endif
%{_bindir}/aklog
%{_bindir}/bos
%{_bindir}/fs
%{_bindir}/klog.krb5
%{_bindir}/livesys
%{_bindir}/pts
%{_bindir}/sys
%{_bindir}/pagsh
%{_bindir}/pagsh.krb
%{_bindir}/restorevol
%{_bindir}/tokens
%{_bindir}/tokens.krb
%{_bindir}/translate_et
%{_bindir}/udebug
%{_bindir}/unlog
%{_bindir}/up
%{_sbindir}/afsbackup
%{_sbindir}/butc
%{_sbindir}/fstrace
%{_sbindir}/rxdebug
%{_sbindir}/vos
%{_libdir}/libafsauthent.so.*
%{_libdir}/libkopenafs.so.*
%{_libdir}/libafsrpc.so.*
%{_libdir}/libafshcrypto.so.*
%{_libdir}/librokenafs.so.*
%{_mandir}/man1/*
%{_mandir}/man3/*
%{_mandir}/man5/*
%{_mandir}/man8/*

%files client
%defattr(-, root, root)
%dir %{_localstatedir}/cache/openafs
%config(noreplace) %{_sysconfdir}/openafs/CellServDB
%config(noreplace) %{_sysconfdir}/openafs/ThisCell
%config(noreplace) %{_sysconfdir}/openafs/cacheinfo
%if 0%{?_with_systemd}
%{_unitdir}/openafs-client.service
%{_libexecdir}/openafs/posthooks.sh
%{_sysconfdir}/openafs/posthooks.d/*.sh
%endif
%{_bindir}/afsio
%{_bindir}/cmdebug
%{_bindir}/xstat_cm_test
%{_sbindir}/afsd

%files server
%defattr(-,root,root)
%if 0%{?_with_systemd}
%{_unitdir}/openafs-server.service
%endif
%{_bindir}/afsmonitor
%{_bindir}/asetkey
%{_bindir}/scout
%{_bindir}/udebug
%{_bindir}/xstat_fs_test
%{_bindir}/akeyconvert
%{_libexecdir}/openafs/buserver
%{_libexecdir}/openafs/dafileserver
%{_libexecdir}/openafs/dasalvager
%{_libexecdir}/openafs/davolserver
%{_libexecdir}/openafs/fileserver
%{_libexecdir}/openafs/ptserver
%{_libexecdir}/openafs/salvager
%{_libexecdir}/openafs/salvageserver
%{_libexecdir}/openafs/upclient
%{_libexecdir}/openafs/upserver
%{_libexecdir}/openafs/vlserver
%{_libexecdir}/openafs/volserver
%{_sbindir}/dafssync-debug
%{_sbindir}/fssync-debug
%{_sbindir}/salvsync-debug
%{_sbindir}/state_analyzer
%{_sbindir}/bosserver
%{_sbindir}/fms
%{_sbindir}/prdb_check
%{_sbindir}/pt_util
%{_sbindir}/read_tape
%{_sbindir}/uss
%{_sbindir}/vlclient
%{_sbindir}/vldb_check
%{_sbindir}/vldb_convert
%{_sbindir}/voldump
%{_sbindir}/volinfo
%{_sbindir}/volscan
%{_sbindir}/bos_util
%{_sbindir}/rmtsysd

%files devel
%defattr(-,root,root)
%{_bindir}/afs_compile_et
%{_bindir}/rxgen
%{_includedir}/afs
%{_includedir}/rx
%{_includedir}/opr
%{_includedir}/*.h
%{_sbindir}/vsys
%{_libdir}/libafsauthent.so
%{_libdir}/libafsrpc.so
%{_libdir}/libkopenafs.so
%{_libdir}/libafshcrypto.so
%{_libdir}/librokenafs.so
%{_libdir}/lib*.a
%{_libdir}/afs
%{_datadir}/openafs/C/afszcm.cat

%files -n dkms-%{name}
%defattr(-,root,root)
%{_prefix}/src/%{name}-%{dkms_version}

%changelog
* Fri Oct  6 2023 Angel Marin <anmar@anmar.eu.org> - 1.8.10-0.1
- Update to OpenAFS 1.8.10

